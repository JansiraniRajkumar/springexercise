package com.jansi.spring.repository;

import org.springframework.data.repository.CrudRepository;

import com.jansi.spring.model.Topic;

public interface TopicRepository extends CrudRepository<Topic,String>{
}
