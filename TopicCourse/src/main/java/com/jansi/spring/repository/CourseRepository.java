package com.jansi.spring.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jansi.spring.model.Course;

public interface CourseRepository extends CrudRepository<Course, String> {
	
	public List<Course> findByTopicId(String topicId);

}
